﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace ConsoleApp5
{
    class Program
    {
     
        static void Main(string[] args)
        {

                Console.WriteLine("startTime | duration");
                TimeSpan[] startTimes = new TimeSpan[5];
                int[] durations = new int[5];
                for (int i = 0; i < 5; i++) {
                    string str = Console.ReadLine();
                    string[] part = str.Split(' ');
                     startTimes[i] = TimeSpan.Parse(part[0]);
                     durations[i] = Convert.ToInt32(part[1]);
                 }


            Console.WriteLine("Working Times");
            string s = Console.ReadLine();
            string[] parts = s.Split('-');
            TimeSpan beginWorkingTime = TimeSpan.Parse(parts[0]);
            TimeSpan endWorkingTime = TimeSpan.Parse(parts[1]);


            Console.WriteLine("Consultation Time");
            int consultationTime = Convert.ToInt32(Console.ReadLine());



            int k = 0;
            while (beginWorkingTime < endWorkingTime)
            {
                TimeSpan check = startTimes[k] + TimeSpan.FromMinutes(durations[k]);
                
                if (beginWorkingTime   < check   &&  beginWorkingTime + TimeSpan.FromMinutes(consultationTime) > startTimes[k])
                {
                    beginWorkingTime = check;
                    k++;
                    if (k >= 5) {
                        k = 4;
                    }
                    continue;

                }
               else  {
                    Console.WriteLine(beginWorkingTime.ToString("hh':'mm") + " " + (beginWorkingTime += TimeSpan.FromMinutes(consultationTime)).ToString("hh':'mm"));
                   
                //   Console.WriteLine(beginWorkingTime);
                 
                }


            } 

        }
    }
}
