﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp3.Pages;

namespace WpfApp3
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DateApp.TradeEntities2 ctx = new DateApp.TradeEntities2();
        int id = 0;
        int role = 0;
        

        public MainWindow()
        {
            InitializeComponent();

        }

        private void ButtonEnter_Click(object sender, RoutedEventArgs e)
        {
            int user = ctx.User.Where(x => (x.UserLogin == login.Text  && x.UserPassword == password.Password)).Count();
          
            if (user != 0) {
                var query = ctx.User.Where(x => (x.UserLogin == login.Text && x.UserPassword == password.Password)).FirstOrDefault();
                id = query.UserID;
                role = query.UserRole;
                Window products = new Pages.Products(id, role);
                products.Show();
                this.Close();
            } else {
               
                MessageBox.Show("Неверный логин или пароль!", "Уведомление", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                Window cap = new Pages.Capctha();
                cap.Show();

            }

        }

        private void LabelGuest_MouseDoubleClick(object sender, MouseButtonEventArgs e) // вход в качестве гостя
        {
            
            Window products = new Pages.Products(id, role);
            products.Show();
            this.Close();
        }
    }
}
