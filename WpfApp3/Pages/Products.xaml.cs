﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp3.DateApp;

namespace WpfApp3.Pages
{
    /// <summary>
    /// Логика взаимодействия для Products.xaml
    /// </summary>
    public partial class Products : Window
    {
       List<NewProduct> lstProduct ;
        int id = 0;
        int role = 0;
        static string article;
        ChangeProducts _dialog; // окно редактирования


        TradeEntities2 ctx = new TradeEntities2(); // база данных
        int Allcount; // количество товаров в бд
        public Products(int id, int role)
        {
            this.id = id;
            this.role = role;
                 
            InitializeComponent();

            InitializeList();

            string[] manufacturer = (from Product in ctx.Product  select Product.ProductManufacturer.ToString()).Distinct().ToArray();
            manufacture.Items.Add("Все производители");
            foreach (string i in manufacturer)
            {
                manufacture.Items.Add(i);
            }


            price.Items.Add("Без сортировки цены");
            price.Items.Add("По возрастанию цены");
            price.Items.Add("По убыванию цены");

        }

        private void InitializeList()
        {
           var query = from Product in ctx.Product select new NewProduct { ImagePreview = Product.ProductPhoto, Article = Product.ProductArticleNumber, Name = Product.ProductName, Description = Product.ProductDescription, Manufactory = Product.ProductManufacturer, Price = Product.ProductCost, Stock = Product.ProductQuantityInStock.ToString(), Bid = Product.ProductAvailable };
            lstProduct = query.ToList();
            list.ItemsSource = query.ToList();
             
            Allcount = query.Count();
            count.Content = Allcount + " из " + Allcount; // вывод количество показанных продуктов
        
        
    }

    private void ButtonFind_Click(object sender, RoutedEventArgs e) // поиск
        {
           

            var query = from Product in ctx.Product where Product.ProductName.Contains(find.Text) || Product.ProductDescription.Contains(find.Text) select new NewProduct{ ImagePreview = Product.ProductPhoto, Article = Product.ProductArticleNumber, Name = Product.ProductName, Description = Product.ProductDescription, Manufactory = Product.ProductManufacturer, Price = Product.ProductCost, Stock = Product.ProductQuantityInStock.ToString(), Bid = Product.ProductAvailable };
            lstProduct = query.ToList();
            list.ItemsSource = query.ToList();
            count.Content = query.Count() + " из " + Allcount;
        }

        public class NewProduct
        {
            public string ImagePreview { get; set; }
            public string Article { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string Manufactory { get; set; }
            public decimal Price { get; set; }

            public string Stock { get; set; }
           public string Bid { get; set; }
           
               
        }

       
        public class Users
        {
            public string FIO{ get; set; }
                         
        }
        private void manufacture_DropDownClosed(object sender, EventArgs e)
        {
            if (manufacture.Text == "Все производители")
            {
                InitializeList();
                
            }
            else
            {
                var proFiltered = from Emp in lstProduct
                                  let ename = Emp.Manufactory
                                  where
                                   ename.Contains(manufacture.Text)
                                   
                                  select Emp;

                list.ItemsSource = proFiltered;
                /*  var query = from Product in ctx.Product where Product.ProductManufacturer.Contains(manufacture.Text) select new NewProduct { ImagePreview = Product.ProductPhoto, Article = Product.ProductArticleNumber, Name = Product.ProductName, Description = Product.ProductDescription, Manufactory = Product.ProductManufacturer, Price = Product.ProductCost, Stock = Product.ProductQuantityInStock.ToString() };
                  list.ItemsSource = query.ToList(); */
                count.Content = list.Items.Count + " из " + Allcount;
            }
        }

        private void ComboBoxPrice_DropDownClosed(object sender, EventArgs e)
        {
            if (price.Text == "Без сортировки цены")
            {

              

            }
            else if (price.Text == "По возрастанию цены")
            {
               ICollectionView view = CollectionViewSource.GetDefaultView(list.ItemsSource);
               view.SortDescriptions.Clear();
                view.SortDescriptions.Add(new SortDescription("Price", ListSortDirection.Ascending));
                view.Refresh();
            //    view.SortDescriptions.Add(new SortDescription("Price", ListSortDirection.Ascending));
              //  view.Filter = new Predicate<object>((o) => ((NewProduct)o).Price..StartsWith(SearchPattern));
                //view.SortDescriptions.Add(new SortDescription("Price", ListSortDirection.Ascending));

              /*  var query = from Product in ctx.Product orderby Product.ProductCost ascending select new NewProduct { ImagePreview = Product.ProductPhoto, Article = Product.ProductArticleNumber, Name = Product.ProductName, Description = Product.ProductDescription, Manufactory = Product.ProductManufacturer, Price = Product.ProductCost.ToString(), Stock = Product.ProductQuantityInStock.ToString() };
                list.ItemsSource = query.ToList(); */
                count.Content = list.Items.Count + " из " + Allcount;
            }
            else 
            {

                ICollectionView view = CollectionViewSource.GetDefaultView(list.ItemsSource);
                view.SortDescriptions.Clear();
                view.SortDescriptions.Add(new SortDescription("Price", ListSortDirection.Descending));
                view.Refresh();

                /* var query = from Product in ctx.Product orderby Product.ProductCost descending select new NewProduct { ImagePreview = Product.ProductPhoto, Article = Product.ProductArticleNumber, Name =  Product.ProductName,  Description = Product.ProductDescription,  Manufactory = Product.ProductManufacturer, Price = Product.ProductCost.ToString(), Stock = Product.ProductQuantityInStock.ToString() };
                 list.ItemsSource = query.ToList();*/
                count.Content = list.Items.Count + " из " + Allcount;

            }
        }

        private void Loaded_Loaded(object sender, RoutedEventArgs e)
        {
        
            if (id > 0) // вывод ФИО пользователя
            {
                var query = ctx.User.Where(u => u.UserID == id).FirstOrDefault();
                person.Content = query.UserSurname + " " + query.UserName + " " + query.UserPatronymic;
            }
            else { // если пользователя нет в бд
                person.Content = " ";
            }
            if (role > 0) // открытие доступа к кнопкам для администратора 
            {
                var query = ctx.Role.Where(u => u.RoleID == role).FirstOrDefault().RoleName;
                if (query == "Администратор")
                {

                }
                else {  // скрытие функционала для других пользователей
                add.Visibility = Visibility.Hidden;
                update.Visibility = Visibility.Hidden;
                delete.Visibility = Visibility.Hidden;
                }
            }
            else // скрытие от гостей
            {
                person.Content = " ";
                add.Visibility = Visibility.Hidden;
                update.Visibility = Visibility.Hidden;
                delete.Visibility = Visibility.Hidden;
            }
        }

        private void return_Click(object sender, RoutedEventArgs e) // возвращение на окно входа
        {
            MainWindow window = new MainWindow();
            window.Show();
            this.Close();
        }

        private void add_Click(object sender, RoutedEventArgs e)
        {

            AddProducts pr = new AddProducts();
            pr.Show();
           pr.Closed += new EventHandler(pr_FormClosed); // обновление после добавления
        }

        private void pr_FormClosed(object sender, EventArgs e)
        {
            InitializeList();
        }

        private void update_Click(object sender, RoutedEventArgs e) // кнопка редактировать
        {
            if (_dialog == null) // запрет открытия нескольких окон редактирования
            {
                _dialog = new ChangeProducts(article);
            }
            _dialog.Show();

            _dialog.Closed += new EventHandler(pr_FormClosed); // обновление при завершении редактирования
        }

        private void list_SelectionChanged(object sender, SelectionChangedEventArgs e) // получение артикля выбранной строки
        {
          

            if (e.AddedItems.Count != 1)
            {
                return; 
            }

            var selected = (NewProduct)e.AddedItems[0];
            article = selected.Article;
            
        }

        private void delete_Click(object sender, RoutedEventArgs e) // удаление товара
        {
            int query = ctx.OrderProduct.Where(x => x.ProductArticleNumber.Equals(article)).Count(); // проверка его наличия в заказах
            if (query != 0)
            {
                MessageBox.Show("Товар, который присутствует в заказе, удалить нельзя.", "Внимание");
            }
            else {
                var del = ctx.Product.Where(x => x.ProductArticleNumber.Equals(article)).FirstOrDefault();
                ctx.Product.Remove(del);
                ctx.SaveChanges();
                InitializeList(); // обновление
            }
        }

        private void exit_Click(object sender, RoutedEventArgs e) // выход из программы
        {

            var result = MessageBox.Show("Вы действительно хотите выйти?", "Внимание", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown(); // завершение приложения
            }
            else { }
         
        }
    }
}
