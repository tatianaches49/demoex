﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp3.Pages
{
    /// <summary>
    /// Логика взаимодействия для Capctha.xaml
    /// </summary>
    public partial class Capctha : Window
    {
        // данный код убирает возможность свернуть, закрыть окно
        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);


        int n = 1;
        Dictionary<int, string> key;
        public Capctha()
        {
            InitializeComponent();
            key = new Dictionary<int, string>()
            {
                [1] = "just",
                [2] = "example",
                [3] = "or not",
                [4] = "robot!",
            };
            img.Source = new BitmapImage(new Uri("/Images/" + n + ".png", UriKind.Relative)) { CreateOptions = BitmapCreateOptions.IgnoreImageCache };
        }

        private void update_Click(object sender, RoutedEventArgs e)
        {
            n++;
            if (n <= 4)
            {

                img.Source = new BitmapImage(new Uri("/Images/" + n + ".png", UriKind.Relative));

                if (n == 4)
                {
                    n = 0;
                }
            }

        }

        private void check_Click(object sender, RoutedEventArgs e)
        {
             if (captchaEdit.Text.Equals(key[n]))
            {
                this.Close();
               
            }
            if (!captchaEdit.Text.Equals(key[n]))
            {
                MessageBox.Show("Текст не совпадает", "Оповещение", MessageBoxButton.OKCancel, MessageBoxImage.Error);
                Window block = new Pages.Block();
                block.Show();
                this.Close();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var hwnd = new WindowInteropHelper(this).Handle;
            SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
        }
    }
}
