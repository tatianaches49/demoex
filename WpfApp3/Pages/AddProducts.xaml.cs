﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp3.DateApp;

namespace WpfApp3.Pages
{
    /// <summary>
    /// Логика взаимодействия для AddProducts.xaml
    /// </summary>
    public partial class AddProducts : Window
    {
        string filepath;
        TradeEntities2 ctx = new TradeEntities2(); // бд
        public AddProducts()
        {
            InitializeComponent();
        }

        private void ButtonPhoto_Click(object sender, RoutedEventArgs e) // загрузка изображения
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Multiselect = false;
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp; *.png)|*.jpg; *.jpeg; *.gif; *.bmp; *.png";
            bool? result = open.ShowDialog();

            if (result == true)
            {
                filepath = open.FileName;   
                ImageSource imgsource = new BitmapImage(new Uri(filepath)); 
                img.Source = imgsource;
            }
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {

            BitmapImage myImg;
            
                System.Uri myUri = new Uri(filepath);
                myImg = new BitmapImage(myUri);

            if (myImg.Width > 300 || myImg.Height > 200)
            {
                MessageBox.Show("Размер фото не должен превышать 300Х200 пикселей", "Уведомление");
            }
         

           else if (Convert.ToInt32(count.Text) < 0 || Convert.ToDecimal(cost.Text) < 0) {
                MessageBox.Show("Значение не может быть отрицательным!", "Уведомление");
            }
            else {
                try
                {
                    Product pr = new Product();
                    pr.ID = (from Product in ctx.Product select Product.ID).Count() + 1;
                    pr.ProductArticleNumber = article.Text;
                    pr.ProductName = name.Text;
                    pr.ProductCategory = category.Text;
                    pr.ProductQuantityInStock = Convert.ToInt32(count.Text);
                    pr.ProductStatus = quantitu.Text;
                    pr.Provider = provider.Text;
                    pr.ProductManufacturer = manufactory.Text;
                    pr.ProductCost = Convert.ToDecimal(cost.Text);
                    pr.ProductDescription = description.Text;
                    pr.ProductPhoto = filepath;
                    ctx.Product.Add(pr);
                    ctx.SaveChanges();
                    MessageBox.Show("Продукт успешно создан!", "Уведомление");
                }
                catch { MessageBox.Show("Не удалось добавить продукт!", "Уведомление", MessageBoxButton.YesNo, MessageBoxImage.Error); }
            }
        }

        private void ButtonReturn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }

}
    