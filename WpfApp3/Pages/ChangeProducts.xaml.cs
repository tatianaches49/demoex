﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp3.DateApp;

namespace WpfApp3.Pages
{
    /// <summary>
    /// Логика взаимодействия для ChangeProducts.xaml
    /// </summary>
    public partial class ChangeProducts : Window
    {
        string article;
        string filepath;
        TradeEntities2 ctx = new TradeEntities2();
        public ChangeProducts(string article)
        {
            InitializeComponent();
            this.article = article;

            string[] category = (from Product in ctx.Product select Product.ProductCategory.ToString()).Distinct().ToArray();

            foreach (string i in category)
            {
                categories.Items.Add(i);
            }

            var selectD = ctx.Product.Where(w => w.ProductArticleNumber == article).FirstOrDefault();
            ID.IsReadOnly = true;
            Article.IsReadOnly = true;
            ID.Text = selectD.ID.ToString();
            Article.Text = selectD.ProductArticleNumber;
            Name.Text = selectD.ProductName;
            Count.Text = selectD.ProductQuantityInStock.ToString();
            Value.Text = selectD.ProductStatus;
            Provider.Text = selectD.Provider;
            Manufactory.Text = selectD.ProductManufacturer;
            Cost.Text = selectD.ProductCost.ToString();
            Description.Text = selectD.ProductDescription;
            this.img.Source = new BitmapImage(new Uri(selectD.ProductPhoto));
            filepath = selectD.ProductPhoto;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_Change(object sender, RoutedEventArgs e)
        {


            BitmapImage myImg;

            System.Uri myUri = new Uri(filepath); // путь к картинке
            myImg = new BitmapImage(myUri);

            if (myImg.Width > 300 || myImg.Height > 200) // проверка размера
            {
                MessageBox.Show("Размер фото не должен превышать 300Х200 пикселей", "Уведомление");
            }

            else if (Convert.ToInt32(Count.Text) < 0 || Convert.ToDecimal(Cost.Text) < 0) // проверка количества и цены на отрицательность
            {
                MessageBox.Show("Значение не может быть отрицательным!", "Уведомление");
            }
            else
            {
                try // обновление
                {
                    var pr = ctx.Product.Where(w => w.ID.ToString() == ID.Text).FirstOrDefault();
                    
                 //   pr.ProductArticleNumber = Article.Text;
                    pr.ProductName = Name.Text;
                    pr.ProductCategory = categories.Text;
                    pr.ProductQuantityInStock = Convert.ToInt32(Count.Text);
                    pr.ProductStatus = Value.Text;
                    pr.Provider = Provider.Text;
                    pr.ProductManufacturer = Manufactory.Text;
                    pr.ProductCost = Convert.ToDecimal(Cost.Text);
                    pr.ProductDescription = Description.Text;
                    pr.ProductPhoto = filepath;

                    ctx.SaveChanges();

                    MessageBox.Show("Данные обновлены!", "Уведомление");
                }
                catch
                {
                    MessageBox.Show("Не удалось совершить обновление!", "Уведомление");
                }
            }
        }

        private void ButtonPhoto_Click(object sender, RoutedEventArgs e) // фото с компьютера
        {
            OpenFileDialog open = new OpenFileDialog(); // открытие для выбора файла
            open.Multiselect = false; // запрет выбора нескольких файлов
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp; *.png)|*.jpg; *.jpeg; *.gif; *.bmp; *.png"; // допустимые разрешения
            bool? result = open.ShowDialog(); // выбор

            if (result == true)
            {
                filepath = open.FileName;  // Сохранение исходного пути в текстовом поле  
                ImageSource imgsource = new BitmapImage(new Uri(filepath)); // необходимо для вывода изображения в форме
                img.Source = imgsource; // загрузка в img 
            }
        }
    }
}
